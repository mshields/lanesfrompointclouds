# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 14:47:15 2019

@author: mshields 
Oxford Technical Solutions Ltd.

Confidential and Proprietary. Copyright © 2019 Oxford Technical Solutions. All rights reserved.

"""

# ##################################################################################################################################
# Importing libs
import oxts_transforms as ot

import pandas as pd
import numpy as np
#from scipy import stats
from collections import OrderedDict

import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
#import seaborn as sns

from sklearn.cluster import DBSCAN
from sklearn import linear_model
from sklearn.decomposition import PCA
#from sklearn import metrics
#from sklearn.preprocessing import StandardScaler

# ##################################################################################################################################
# Options <------------------------------------------------------------------------------------------------------------------------- Needs arguementifying

# Input filename and path
file = "mobile.txt"
root = "O:\\Application Engineering\\Projects\\COMPLETED\\AE107 Extracting Collateral for RT-Range from Point Clouds\\Upper Heyford Scan\\Point Cloud"
#root = "C:\\Users\\mshields\\Desktop\\Box Sync\\080_Point Cloud Classification\\RAF Honnington Scan\\Point Cloud"

#file = "Test Data\\test2016102703_Sample01a_split_10.txt"
#root = "C:\\Users\\mshields\\Desktop\\Box Sync\\080_Point Cloud Classification\\"

# Setting resolution for plots
dpi = 400
axes = plt.gca()

# Machine Learning model parameters (m)
line_width = 0.15
lane_width = 5.4
road_width = 70

# Leap seconds
leapsec = 18

# Time thresholds
min_t = 0
max_t = 0

# X Position thresholds
min_x = 350 # -700 for RAF Honnington
max_x = 700

# Intensity thresholds
min_intensity = 12 #9 #45
max_intensity = 80 #29 #80

# Line and map file point spacing (m)
min_distance = 1

# Length of map file start line (m)
sl_length = 140

# Point index of Line 4 to be used as the centre of start line
sl_point_ind = 3

#Defining output path
output_path = "O:\\Application Engineering\\Projects\\COMPLETED\\AE107 Extracting Collateral for RT-Range from Point Clouds\\Results\Upper Heyford"
#output_path = "C:\\Users\\mshields\\Desktop\\Box Sync\\080_Point Cloud Classification\\Results\\RAF Honnington"

# .lrf file location
lrfloc = "O:\\Application Engineering\\Projects\\COMPLETED\\AE107 Extracting Collateral for RT-Range from Point Clouds\\Upper Heyford Scan\\Navigation Data\\ConfigurationFromRawData\\190416_153455.lrf"
#lrfloc = "C:\\Users\\mshields\\Desktop\\Box Sync\\080_Point Cloud Classification\\RAF Honnington Scan\\Navigation Data\\Configuration\\mobile.lrf"

# KML options
kml_col = "FF00FF00"
kml_size = str(1)

# ##################################################################################################################################
# Read in

# Defining read in function
def readlastxt(root, file):

    # Read in text file
    raw = open(root + "//" + file, "r").read().splitlines()
    
    # Make each line a string in an array
    data = np.array(raw)
    del raw
    
    # Set the seperator for the text file
    sep = ","
    
    #Split the strings and turn them into floats
    data = OrderedDict([ ('GPS time [s]', [float(i.split(sep)[0]) for i in data]),
                        ('X Pos', [float(i.split(sep)[1]) for i in data]),
                        ('Y Pos',  [float(i.split(sep)[2]) for i in data]),
                        ('Z Pos', [float(i.split(sep)[3]) for i in data]),
                        ('Intensity', [abs(float(i.split(sep)[4])) for i in data]) ])
    
    # Create a dataframe
    df = pd.DataFrame.from_dict(data)
    
    # Convert from nanoseconds to seconds
    df['GPS time [s]'] = df['GPS time [s]']*10**-9
    
    return(df)

# Reading data
df = readlastxt(root, file)

# ##################################################################################################################################
# Read in .lrf file
lrftxt = open(lrfloc, "r").read().splitlines()

# Define origin
lat_o = float(lrftxt[0])
lon_o = float(lrftxt[1])
alt_o = float(lrftxt[2])
hea_o = float(lrftxt[3])

# Clean up
del lrfloc, lrftxt

# ##################################################################################################################################
# Assigning point IDs

#df['Point ID'] = range(len(df))

# ##################################################################################################################################
# Using RANSAC estimation for a ground plane

ransac = linear_model.RANSACRegressor(linear_model.LinearRegression())

ransac.fit(df.loc[:, ['X Pos', 'Y Pos']], df['Z Pos'])

df['Ground Plane'] = ransac.predict(df.loc[:, ['X Pos', 'Y Pos']])

# Clean up
del ransac

# ##################################################################################################################################
# Setting ground tolerance as a percentage of the global height<------------------------------------Might need to put this back on if data has buildings or similar
#deltaZ = max(df['Z Pos']) - min(df['Z Pos'])
#groundtolerance = 0.03 * deltaZ

# Setting the ground tolerance to within 20 cm of the ground plane
groundtolerance = 0.2

# Tagging Ground 1 = Ground 0 = Not Ground
df['Ground'] = 0

df.loc[df['Z Pos'] <= (df['Ground Plane'] + groundtolerance), 'Ground'] = 1

# Clean up
del groundtolerance#, deltaZ

# ##################################################################################################################################
# Exploratory Plots

# Projected Plots
"""
# Ground Projected Plot
plt.subplot(2, 2, 3, adjustable='datalim', aspect='equal')
plt.scatter(df['X Pos'], df['Y Pos'], c=df['Ground'], marker='.', alpha=0.5, s=0.1)
plt.xlabel('x')
plt.ylabel('y')
plt.axis('off')

plt.subplot(2, 2, 4, adjustable='datalim', aspect='equal')
plt.scatter(df['Z Pos'], df['Y Pos'], c=df['Ground'], marker='.', alpha=0.5, s=0.1)
plt.xlabel('z')
plt.ylabel('y')
plt.axis('off')

plt.subplot(2, 2, 1, adjustable='datalim', aspect='equal')
plt.scatter(df['X Pos'], df['Z Pos'], c=df['Ground'], marker='.', alpha=0.5, s=0.1)
plt.xlabel('x')
plt.ylabel('z')
plt.axis('off')

#plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - Ground Segmentation.png', 
#            dpi=dpi)
plt.show()

# Intensity Projected Plot
plt.subplot(2, 2, 3, adjustable='datalim', aspect=1)
plt.scatter(df['X Pos'], df['Y Pos'], c=df['Intensity'], marker='.', alpha=0.5, s=0.1)
plt.gray()
plt.xlabel('x')
plt.ylabel('y')
plt.axis('off')

plt.subplot(2, 2, 4, adjustable='datalim', aspect=1)
plt.scatter(df['Z Pos'], df['Y Pos'], c=df['Intensity'], marker='.', alpha=0.5, s=0.1)
plt.gray()
plt.xlabel('z')
plt.ylabel('y')
plt.axis('off')

plt.subplot(2, 2, 1, adjustable='datalim', aspect=1)
plt.scatter(df['X Pos'], df['Z Pos'], c=df['Intensity'], marker='.', alpha=0.5, s=0.1)
plt.gray()
plt.xlabel('x')
plt.ylabel('z')
plt.axis('off')

#plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - Intensity.png', 
            #dpi=dpi)
plt.show()
"""

# Individual Plots
"""
# XY Plane Plot
plt.scatter(df['X Pos'], 
            df['Y Pos'], 
            c=df['Ground'], 
            alpha=0.5, 
            marker='.', 
            s=0.1)
plt.title('XY Plane')
plt.xlabel('X Pos')
plt.ylabel('Y Pos')
plt.axis('off')
plt.axes().set_aspect('equal', 'datalim')
#plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - Ground Segmentation XY.png', 
#            dpi=dpi)
plt.show()

# XZ Plane Plot
plt.scatter(df['X Pos'], 
            df['Z Pos'], 
            c=df['Ground'], 
            alpha=0.5, 
            marker='.', 
            s=0.1)
plt.title('XZ Plane')
plt.xlabel('X Pos')
plt.ylabel('Z Pos')
plt.axis('off')
plt.axes().set_aspect('equal', 'datalim')
plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - Ground Segmentation XZ.png', 
            dpi=dpi)
#plt.show()

# YZ Plane Plot
plt.scatter(df['Y Pos'], 
            df['Z Pos'], 
            c=df['Ground'], 
            alpha=0.5, 
            marker='.', 
            s=0.1)
plt.title('YZ Plane')
plt.xlabel('Y Pos')
plt.ylabel('Z Pos')
plt.axis('off')
plt.axes().set_aspect('equal', 'datalim')
plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - Ground Segmentation YZ.png', 
            dpi=dpi)
#plt.show()
"""

"""   -------------------------------------RAF Honnington PCA stuff????
df = df[df['Intensity'] >= min_intensity] #np.mean(df['Intensity'])]
df = df[df['Intensity'] <= max_intensity]

df = df[df['X Pos'] >= -700]
df = df[df['Y Pos'] <= -55]


M = np.mean(X, axis=0)

# center columns by subtracting column means
C = X - M

# calculate covariance matrix of centered matrix
V = np.cov(X.T)

values, vectors = np.linalg.eig(V)
print(vectors)
print(values)

P = vectors.T.dot(C.T)

def draw_vector(v0, v1, ax=None):
    ax = ax or plt.gca()
    arrowprops=dict(arrowstyle='->',
                    linewidth=2,
                    shrinkA=0, shrinkB=0)
    ax.annotate('', v1, v0, arrowprops=arrowprops)

# plot data
plt.scatter(X[:, 0], X[:, 1], alpha=0.2)
for length, vector in zip(pca.explained_variance_, pca.components_):
    v = vector * 3 * np.sqrt(length)
    draw_vector(pca.mean_, pca.mean_ + v)
plt.axis('equal');

# XY Plane Intensity Plot
plt.scatter(df['X Pos'], 
            df['Y Pos'], 
            c=df['Intensity'], 
            alpha=0.5, 
            marker='.', 
            s=0.1)
plt.gray()
plt.title('XY Plane Intensity')
plt.xlabel('X Pos')
plt.ylabel('Y Pos')
plt.axis('off')
plt.axes().set_aspect('equal', 'datalim')
#plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - Intensity XY.png', 
#            dpi=dpi)
plt.show()

"""

"""
# XZ Plane Intensity Plot
plt.scatter(df['X Pos'], 
            df['Z Pos'], 
            c=df['Intensity'], 
            alpha=0.5, 
            marker='.', 
            s=0.1)
plt.gray()
plt.title('XZ Plane Intensity')
plt.xlabel('X Pos')
plt.ylabel('Z Pos')
plt.axis('off')
plt.axes().set_aspect('equal', 'datalim')
plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - Intensity XZ.png', 
            dpi=dpi)
#plt.show()

# YZ Plane Intensity Plot
plt.scatter(df['Y Pos'], 
            df['Z Pos'], 
            c=df['Intensity'], 
            alpha=0.5, 
            marker='.', 
            s=0.1)
plt.gray()
plt.title('YZ Plane Intensity')
plt.xlabel('Y Pos')
plt.ylabel('Z Pos')
plt.axis('off')
plt.axes().set_aspect('equal', 'datalim')
plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - Intensity YZ.png', 
            dpi=dpi)
#plt.show()
"""
# ##################################################################################################################################
# Intensity filter

#sns.distplot(df['Intensity'])

df = df[df['Intensity'] >= min_intensity] #np.mean(df['Intensity'])]
df = df[df['Intensity'] <= max_intensity]

#sns.distplot(df['Intensity'])

# ##################################################################################################################################
# Sub setting <---------------------------------------------------------------------------------------------------------------------Won't need this for other data sets
# Trimming on X values
df = df[df['X Pos'] <= max_x]
df = df[df['X Pos'] >= min_x]

# Sort on X values ###### <------------------------------------------------------------------------------------------------------------------ might want this further up
df = df.sort_values(by=['X Pos'])

# Extracting ground points for clustering
X = df.loc[df['Ground'] == 1, ['X Pos', 'Y Pos']] # <-----------------------------------------------------------------------Might need to put this back on if data has buildings or similar

# Pre-processing for ML

# Convert to array
X = df.loc[:, ['X Pos', 'Y Pos']].as_matrix()

# Normalise
mins = np.min(X, axis=0)

X = X - mins

# ##################################################################################################################################
# Compute DBSCAN
db = DBSCAN(eps=10, min_samples=200, metric='euclidean').fit(X)

core_samples_mask = np.zeros_like(db.labels_, dtype=bool)

core_samples_mask[db.core_sample_indices_] = True

labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)

#print('Estimated number of clusters: %d' % n_clusters_)
#print('Estimated number of noise points: %d' % n_noise_)

# ##################################################################################################################################
# Add clustering labels and removing noise points (label = -1)
#X[:, 2] =  labels
X =  np.column_stack((X, labels))

X = X[X[:, 2] != -1]

# ##################################################################################################################################
# Using RANSAC inliers on all lanes to eliminate noise
ransac = linear_model.RANSACRegressor(residual_threshold = road_width)#residual_threshold=line_width)

ransac.fit(X[:, 0].reshape(-1, 1), X[:, 1].reshape(-1, 1))

#X = np.column_stack((X, ransac.predict(X[:, 0].reshape(-1, 1))))

X = X[ransac.inlier_mask_]

# ##################################################################################################################################
# Plot result
"""
plt.scatter(X[:,0], X[:,1], c=X[:,2], cmap='Set2', marker='.', s=0.1)
plt.title('XY Plane')
plt.xlabel('X Pos')
plt.ylabel('Y Pos')
plt.axis('off')
plt.axes().set_aspect('equal', 'datalim')
#plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - XY.png', dpi=dpi)
plt.show()

plt.scatter(X[:,0], X[:,1], c='b', marker='.', s=0.1)
#plt.plot(X[:,0], X[:,3], c='r', alpha=0.6)
plt.title('Road Markings XY Plane')
plt.xlabel('X Pos')
plt.ylabel('Y Pos')
plt.axis('off')
plt.axes().set_aspect('equal', 'datalim')
#plt.savefig(output_path + "\\"+ file.split(".")[0] + ' - XY RANSAC Line.png', dpi=dpi)
plt.show()
"""
# ##################################################################################################################################
# Modified k-means lane classifier

# Create a matrix of X values and ones
A = np.vstack([X[:,0], np.ones(len(X))]).T

# Find straight line variables for whole data set
#m, c = np.linalg.lstsq(A, X[:,1])[0]

ransac = linear_model.RANSACRegressor(residual_threshold = line_width)

ransac.fit(X[:, 0].reshape(-1, 1), X[:, 1].reshape(-1, 1))

m = ransac.estimator_.coef_[0][0]
c = ransac.estimator_.intercept_[0]

# Offset bias values for the number of expected clusters
c1 = c-15 #<----------------------------------------------------------------------------------------Could use PCA to align data with X axis, such that m=1, then make c relative to the lane width
c2 = c-7
c3 = c-3
c4 = c
c5 = c+3
c6 = c+7
c7 = c+15

# Iterate k-means
k = 7   #<------------------------------------------------------------------------------------------Be nice to have this be an option and functionify the clustering
        #<------------------------------------------------------------------------------------------Would also make writing out to the map file easier as you can iterate k times
        #<------------------------------------------------------------------------------------------Try with multi-dimensional array?
for i in range(10):
    # Calculate residual y value abs(prediction - actual)
    y1 = np.absolute(A.dot([[m],[c1]]) - X[:,[1]])
    y2 = np.absolute(A.dot([[m],[c2]]) - X[:,[1]])
    y3 = np.absolute(A.dot([[m],[c3]]) - X[:,[1]])
    y4 = np.absolute(A.dot([[m],[c4]]) - X[:,[1]])
    y5 = np.absolute(A.dot([[m],[c5]]) - X[:,[1]])
    y6 = np.absolute(A.dot([[m],[c6]]) - X[:,[1]])
    y7 = np.absolute(A.dot([[m],[c7]]) - X[:,[1]])
    
    # Creating input mask for each cluster
    k1 = np.squeeze(np.all([y1<y2,y1<y3,y1<y4,y1<y5,y1<y6,y1<y7],axis=0))
    k2 = np.squeeze(np.all([y2<y1,y2<y3,y2<y4,y2<y5,y2<y6,y2<y7],axis=0))
    k3 = np.squeeze(np.all([y3<y1,y3<y2,y3<y4,y3<y5,y3<y6,y3<y7],axis=0))
    k4 = np.squeeze(np.all([y4<y1,y4<y2,y4<y3,y4<y5,y4<y6,y4<y7],axis=0))
    k5 = np.squeeze(np.all([y5<y1,y5<y2,y5<y3,y5<y4,y5<y6,y5<y7],axis=0))
    k6 = np.squeeze(np.all([y6<y1,y6<y2,y6<y3,y6<y4,y6<y5,y6<y7],axis=0))
    k7 = np.squeeze(np.all([y7<y1,y7<y2,y7<y3,y7<y4,y7<y5,y7<y6],axis=0))
    
    # Update
    A1 = np.vstack([X[k1,0], np.ones(len(X[k1]))]).T
    A2 = np.vstack([X[k2,0], np.ones(len(X[k2]))]).T
    A3 = np.vstack([X[k3,0], np.ones(len(X[k3]))]).T
    A4 = np.vstack([X[k4,0], np.ones(len(X[k4]))]).T
    A5 = np.vstack([X[k5,0], np.ones(len(X[k5]))]).T
    A6 = np.vstack([X[k6,0], np.ones(len(X[k6]))]).T
    A7 = np.vstack([X[k7,0], np.ones(len(X[k7]))]).T

    # Generate straight line to use in distance metric for each cluster
    m1, c1 = np.linalg.lstsq(A1, X[k1,1])[0]
    m2, c2 = np.linalg.lstsq(A2, X[k2,1])[0]
    m3, c3 = np.linalg.lstsq(A3, X[k3,1])[0]
    m4, c4 = np.linalg.lstsq(A4, X[k4,1])[0]
    m5, c5 = np.linalg.lstsq(A5, X[k5,1])[0]
    m6, c6 = np.linalg.lstsq(A6, X[k6,1])[0]
    m7, c7 = np.linalg.lstsq(A7, X[k7,1])[0]
    
    # Generate new mean slope
    m = (m1 + m2 + m + m4 + m5 + m6 + m7) / 7.0
    """
    # Plot each iteration
    plt.scatter(X[:,0], X[:,1], c='b', alpha=0.5, marker='.', s=0.1)
    plt.plot(X[:,0], m*X[:,0] + c1, c='r', alpha=0.5, linewidth=1)
    plt.plot(X[:,0], m*X[:,0] + c2, c='r', alpha=0.5, linewidth=1)
    plt.plot(X[:,0], m*X[:,0] + c3, c='r', alpha=0.5, linewidth=1)
    plt.plot(X[:,0], m*X[:,0] + c4, c='r', alpha=0.5, linewidth=1)
    plt.plot(X[:,0], m*X[:,0] + c5, c='r', alpha=0.5, linewidth=1)
    plt.plot(X[:,0], m*X[:,0] + c6, c='r', alpha=0.5, linewidth=1)
    plt.plot(X[:,0], m*X[:,0] + c7, c='r', alpha=0.5, linewidth=1)
    plt.title('Lane Separation Segment')
    plt.axes().set_aspect('equal', 'datalim')
    plt.axis('off')
    #plt.savefig(output_path + "\\"+ file.split(".")[0] + " - XY Lane Separation Segment.png", dpi=dpi)
    plt.show()
    #"""

# Divide into k clusters
xy1 = X[k1]
xy2 = X[k2]
xy3 = X[k3]
xy4 = X[k4]
xy5 = X[k5]
xy6 = X[k6]
xy7 = X[k7]

# Clean up
del A, m, c, c1, c2, c3, c4, c5, c6, c7, y1, y2, y3, y4, y5, y6, y7, k1, k2, k3, k4, k5, k6, k7, A1, A2, A3, A4, A5, A6, A7, m1, m2, m3, m4, m5, m6, m7, i

# Plot clustered lanes
plt.scatter(xy1[:,0], xy1[:,1], c='r', alpha=0.5, marker='.', s=0.1)
plt.scatter(xy2[:,0], xy2[:,1], c='g', alpha=0.5, marker='.', s=0.1)
plt.scatter(xy3[:,0], xy3[:,1], c='b', alpha=0.5, marker='.', s=0.1)
plt.scatter(xy4[:,0], xy4[:,1], c='c', alpha=0.5, marker='.', s=0.1)
plt.scatter(xy5[:,0], xy5[:,1], c='m', alpha=0.5, marker='.', s=0.1)
plt.scatter(xy6[:,0], xy6[:,1], c='y', alpha=0.5, marker='.', s=0.1)
plt.scatter(xy7[:,0], xy7[:,1], c='k', alpha=0.5, marker='.', s=0.1)
plt.title(file.split(".")[0] + ' Clustered Lanes')
plt.axes().set_aspect('equal', 'datalim')
plt.axis('off')
plt.savefig(output_path + "\\"+ file.split(".")[0] + " - Clusters.png", dpi=dpi)
plt.show()

# ##################################################################################################################################
# RANSAC line fit for each cluster
# Functuion to fit lines
def ransac_line(xy):
    ransac = linear_model.RANSACRegressor(residual_threshold = line_width)
    
    ransac.fit(xy[:,0].reshape(-1, 1), xy[:,1].reshape(-1, 1))
    
    out = ransac.predict(xy[:,0].reshape(-1, 1))
    
    out = np.array([row[0] for row in out])
    
    return(out)

# Fit lines
a = ransac_line(xy1)
b = ransac_line(xy2)
c = ransac_line(xy3)
d = ransac_line(xy4)
e = ransac_line(xy5)
f = ransac_line(xy6)
g = ransac_line(xy7)

# Plot cliusters and overlay polylines

linalp = 0.7
lincol = "k"
pointalp = 0.05

plt.scatter(xy1[:,0], xy1[:,1], c='r', alpha=pointalp, marker='.', s=0.1)
plt.scatter(xy2[:,0], xy2[:,1], c='g', alpha=pointalp, marker='.', s=0.1)
plt.scatter(xy3[:,0], xy3[:,1], c='b', alpha=pointalp, marker='.', s=0.1)
plt.scatter(xy4[:,0], xy4[:,1], c='c', alpha=pointalp, marker='.', s=0.1)
plt.scatter(xy5[:,0], xy5[:,1], c='m', alpha=pointalp, marker='.', s=0.1)
plt.scatter(xy6[:,0], xy6[:,1], c='y', alpha=pointalp, marker='.', s=0.1)
plt.scatter(xy7[:,0], xy7[:,1], c='k', alpha=pointalp, marker='.', s=0.1)
plt.plot(xy1[:, 0], a, c=lincol, alpha=linalp, linewidth=0.3)
plt.plot(xy2[:, 0], b, c=lincol, alpha=linalp, linewidth=0.3)
plt.plot(xy3[:, 0], c, c=lincol, alpha=linalp, linewidth=0.3)
plt.plot(xy4[:, 0], d, c=lincol, alpha=linalp, linewidth=0.3)
plt.plot(xy5[:, 0], e, c=lincol, alpha=linalp, linewidth=0.3)
plt.plot(xy6[:, 0], f, c=lincol, alpha=linalp, linewidth=0.3)
plt.plot(xy7[:, 0], g, c=lincol, alpha=linalp, linewidth=0.3)
#plt.plot(X[:,0], pol1[0]*X[:,0]**2 + pol1[1]*X[:,0] + pol1[2], c='r', alpha=0.6, linewidth=0.2)
#plt.plot(X[:,0], pol2[0]*X[:,0]**2 + pol2[1]*X[:,0] + pol2[2], c='r', alpha=0.6, linewidth=0.2)
plt.title(file.split(".")[0] + ' Lane Lines')
plt.axes().set_aspect('equal', 'datalim')
plt.axis('off')
#plt.savefig(root + 'Results\\' + file.split('\\')[1].split('_')[0] + '_' + file.split('\\')[1].split('_')[3].split('.')[0] + ' - XY Lane Lines.png', dpi=dpi)
plt.savefig(output_path + "\\"+ file.split(".")[0] + " - XY Lane Lines.png", dpi=dpi)
plt.show()


# ##################################################################################################################################
# De-normalise and convert to global frame
line_output1 = ot.loc2lla(a + mins[1], xy1[:,0]+mins[0], alt_o, lat_o, lon_o, alt_o)
line_output2 = ot.loc2lla(b + mins[1], xy2[:,0]+mins[0], alt_o, lat_o, lon_o, alt_o)
line_output3 = ot.loc2lla(c + mins[1], xy3[:,0]+mins[0], alt_o, lat_o, lon_o, alt_o)
line_output4 = ot.loc2lla(d + mins[1], xy4[:,0]+mins[0], alt_o, lat_o, lon_o, alt_o)
line_output5 = ot.loc2lla(e + mins[1], xy5[:,0]+mins[0], alt_o, lat_o, lon_o, alt_o)
line_output6 = ot.loc2lla(f + mins[1], xy6[:,0]+mins[0], alt_o, lat_o, lon_o, alt_o)
line_output7 = ot.loc2lla(g + mins[1], xy7[:,0]+mins[0], alt_o, lat_o, lon_o, alt_o)

# Exporting Lat and Lon only
line_output1 = np.array([line_output1[1], line_output1[0]])
line_output2 = np.array([line_output2[1], line_output2[0]])
line_output3 = np.array([line_output3[1], line_output3[0]])
line_output4 = np.array([line_output4[1], line_output4[0]])
line_output5 = np.array([line_output5[1], line_output5[0]])
line_output6 = np.array([line_output6[1], line_output6[0]])
line_output7 = np.array([line_output7[1], line_output7[0]])

# Transpose for CSV output
line_output1 = np.sort(np.transpose(line_output1), axis=0)
line_output2 = np.sort(np.transpose(line_output2), axis=0)
line_output3 = np.sort(np.transpose(line_output3), axis=0)
line_output4 = np.sort(np.transpose(line_output4), axis=0)
line_output5 = np.sort(np.transpose(line_output5), axis=0)
line_output6 = np.sort(np.transpose(line_output6), axis=0)
line_output7 = np.sort(np.transpose(line_output7), axis=0)

# #################################################################################################################################
# Write out CSV
#pd.DataFrame(line_output1).to_csv("C:\\Users\\mshields\\Desktop\\Pol 1.csv", header=None, index=None)
#pd.DataFrame(line_output2).to_csv("C:\\Users\\mshields\\Desktop\\Pol 2.csv", header=None, index=None)

# #################################################################################################################################
# Line File Output
# Minimum distance between points allowed in a map file is half a metre

# Function to find points in an array that are at least min_distance apart
def distant_point(line_output):
    vec = np.array([0])
    
    i = 1
    
    j = i+1
    
    # Loop over all points
    while i < (len(line_output)-10) and j < len(line_output)-3:
        
        j = i+1
        
        # Calculate havesine distance between two points
        dist = ot.hsdist(line_output[i, 1], line_output[i, 0], line_output[j, 1], line_output[j, 0])
        
        # Iterate until distance is greater than one metre
        while dist <= min_distance and j < len(line_output)-3:
            j = j+1
            
            dist = ot.hsdist(line_output[i, 1], line_output[i, 0], line_output[j, 1], line_output[j, 0])
            
        # Store the indices for that point in a vector
        vec = np.append(vec, j)
        
        i = j
        
    return(vec)

# Function to write line files
def write_line_file(full_path_to_file, line_output):
    
    # Find all point min_distance apart
    vec = distant_point(line_output)
     
    # Writing to line file
    f = open(full_path_to_file,"w+")
    
    for i in range(len(vec)-1):
        hea = round(ot.bear(line_output[vec[i], 1], line_output[vec[i], 0], line_output[vec[i+1], 1], line_output[vec[i+1], 0]) , 4)
        
        if hea > 360:
            hea = hea - 360
        
        f.write(str(line_output[vec[i], 1]) + "," + str(line_output[vec[i], 0]) + "," + str(alt_o) + "," + str(hea) + "," + "\n")
    
    f.close()

# Line 1
write_line_file(output_path + "\\Lidar Line 1.rtrl", line_output1)
write_line_file(output_path + "\\Lidar Line 2.rtrl", line_output2)
write_line_file(output_path + "\\Lidar Line 3.rtrl", line_output3)
write_line_file(output_path + "\\Lidar Line 4.rtrl", line_output4)
write_line_file(output_path + "\\Lidar Line 5.rtrl", line_output5)
write_line_file(output_path + "\\Lidar Line 6.rtrl", line_output6)
write_line_file(output_path + "\\Lidar Line 7.rtrl", line_output7)
#"""
# ##################################################################################################################################
# Map File Output
vec1 = distant_point(line_output1)
vec2 = distant_point(line_output2)
vec3 = distant_point(line_output3)
vec4 = distant_point(line_output4)
vec5 = distant_point(line_output5)
vec6 = distant_point(line_output6)
vec7 = distant_point(line_output7)

# Defining start line based on line 4, sl_point_ind and sl_length <----------------------------------------------------------Range might edit this if the start line isn't beyond the start of all lanes
hea = round(ot.bear(line_output4[vec4[sl_point_ind], 1], line_output4[vec4[sl_point_ind], 0], line_output4[vec4[sl_point_ind+1], 1], line_output4[vec4[sl_point_ind+1], 0]) , 4)

if hea > 360:
    hea = hea - 360 #<---------------------------------------------------------------------------------Probably needs more quadrant checks here

slp1 = ot.llatrans(line_output4[vec4[sl_point_ind], 1], line_output4[vec4[sl_point_ind], 0], (sl_length/2), hea-90)
slp2 = ot.llatrans(line_output4[vec4[sl_point_ind], 1], line_output4[vec4[sl_point_ind], 0], (sl_length/2), hea+90)

# Map File Output
f = open(output_path + "\\Lidar Map.rtrm","w+")

f.write("map number,1\n" +
        "origin," + str(lat_o) + "," + str(lon_o) + "," + str(alt_o) + "\n" +
        "start line," + str(slp1[0]) + "," + str(slp1[1]) + "," +
        str(slp2[0]) + "," + str(slp2[1]) + ",Lidar Line 1.rtrl," + str(sl_point_ind) + "," + str(sl_length) + "\n" + 
        "number of lines," + str(k) + "\n")

# Line 1
f.write("\n"
        "line,1,Lidar Line 1.rtrl" + "\n"
        "number of points," + str(len(vec1)) + "\n")

for i in range(len(vec1)-1):
    hea = round(ot.bear(line_output1[vec1[i], 1], line_output1[vec1[i], 0], line_output1[vec1[i+1], 1], line_output1[vec1[i+1], 0]) , 4)
    
    if hea > 360:
        hea = hea - 360
    
    f.write(str(line_output1[vec1[i], 1]) + "," + str(line_output1[vec1[i], 0]) + "," + str(alt_o) + "," + str(hea) + "," + "\n")

# Line 2
f.write("\n"
        "line,2,Lidar Line 2.rtrl" + "\n"
        "number of points," + str(len(vec2)) + "\n")

for i in range(len(vec2)-1):
    hea = round(ot.bear(line_output2[vec2[i], 1], line_output2[vec2[i], 0], line_output2[vec2[i+1], 1], line_output2[vec2[i+1], 0]) , 4)
    
    if hea > 360:
        hea = hea - 360
    
    f.write(str(line_output2[vec2[i], 1]) + "," + str(line_output2[vec2[i], 0]) + "," + str(alt_o) + "," + str(hea) + "," + "\n")

# Line 3
f.write("\n"
        "line,3,Lidar Line 3.rtrl" + "\n"
        "number of points," + str(len(vec3)) + "\n")

for i in range(len(vec3)-1):
    hea = round(ot.bear(line_output3[vec3[i], 1], line_output3[vec3[i], 0], line_output3[vec3[i+1], 1], line_output3[vec3[i+1], 0]) , 4)
    
    if hea > 360:
        hea = hea - 360
    
    f.write(str(line_output3[vec3[i], 1]) + "," + str(line_output3[vec3[i], 0]) + "," + str(alt_o) + "," + str(hea) + "," + "\n")

# Line 4
f.write("\n"
        "line,4,Lidar Line 4.rtrl" + "\n"
        "number of points," + str(len(vec4)) + "\n")

for i in range(len(vec4)-1):
    hea = round(ot.bear(line_output4[vec4[i], 1], line_output4[vec4[i], 0], line_output4[vec4[i+1], 1], line_output4[vec4[i+1], 0]) , 4)
    
    if hea > 360:
        hea = hea - 360
    
    f.write(str(line_output4[vec4[i], 1]) + "," + str(line_output4[vec4[i], 0]) + "," + str(alt_o) + "," + str(hea) + "," + "\n")

# Line 5
f.write("\n"
        "line,5,Lidar Line 5.rtrl" + "\n"
        "number of points," + str(len(vec5)) + "\n")

for i in range(len(vec5)-1):
    hea = round(ot.bear(line_output5[vec5[i], 1], line_output5[vec5[i], 0], line_output5[vec5[i+1], 1], line_output5[vec5[i+1], 0]) , 4)
    
    if hea > 360:
        hea = hea - 360
    
    f.write(str(line_output5[vec5[i], 1]) + "," + str(line_output5[vec5[i], 0]) + "," + str(alt_o) + "," + str(hea) + "," + "\n")

# Line 6
f.write("\n"
        "line,6,Lidar Line 6.rtrl" + "\n"
        "number of points," + str(len(vec6)) + "\n")

for i in range(len(vec6)-1):
    hea = round(ot.bear(line_output6[vec6[i], 1], line_output6[vec6[i], 0], line_output6[vec6[i+1], 1], line_output6[vec6[i+1], 0]) , 4)
    
    if hea > 360:
        hea = hea - 360
    
    f.write(str(line_output6[vec6[i], 1]) + "," + str(line_output6[vec6[i], 0]) + "," + str(alt_o) + "," + str(hea) + "," + "\n")

# Line 7
f.write("\n"
        "line,7,Lidar Line 7.rtrl" + "\n"
        "number of points," + str(len(vec7)) + "\n")

for i in range(len(vec7)-1):
    hea = round(ot.bear(line_output7[vec7[i], 1], line_output7[vec7[i], 0], line_output7[vec7[i+1], 1], line_output7[vec7[i+1], 0]) , 4)
    
    if hea > 360:
        hea = hea - 360
    
    f.write(str(line_output7[vec7[i], 1]) + "," + str(line_output7[vec7[i], 0]) + "," + str(alt_o) + "," + str(hea) + "," + "\n")


f.close()
#"""
# ##################################################################################################################################
# KML File Output

f = open(output_path + "\\Lidar Map.kml","w+")

f.write("<?xml version=\"1.0\" standalone=\"yes\"?>" + "\n"
        "<kml xmlns=\"http://www.opengis.net/kml/2.2\">" + "\n"
        "<Document>" + "\n")

f.write("<Folder>" + "\n"
        "<name>" + file + "(" + root + ")" + "</name>" + "\n"
        "<open>0</open>" + "\n"
        "<visibility>1</visibility>" + "\n"
        "<Placemark>" + "\n"
        "<name>Line 1</name>" + "\n"
        "<MultiGeometry>" + "\n"
        "<LineString>" + "\n"
        "<altitudeMode>clampToGround</altitudeMode>" + "\n"
        "<coordinates>" + "\n")

for i in range(len(line_output1)):
     f.write(str(line_output1[i, 0]) + "," + str(line_output1[i, 1]) + "\n")

f.write("</coordinates>" + "\n"
        "</LineString>" + "\n"
        "</MultiGeometry>" + "\n"
        "<Style>" + "\n"
        "<LineStyle>" + "\n"
        "<color>" + kml_col + "</color>" + "\n"
        "<width>" + kml_size + "</width>" + "\n"
        "</LineStyle>" + "\n"
        "</Style>" + "\n"
        "</Placemark>" + "\n"
        "</Folder>" + "\n"
        "<Folder>" + "\n"
        "<name>" + file + "(" + root + ")" + "</name>" + "\n"
        "<open>0</open>" + "\n"
        "<visibility>1</visibility>" + "\n"
        "<Placemark>" + "\n"
        "<name>Line 2</name>" + "\n"
        "<MultiGeometry>" + "\n"
        "<LineString>" + "\n"
        "<altitudeMode>clampToGround</altitudeMode>" + "\n"
        "<coordinates>" + "\n")

for i in range(len(line_output2)):
     f.write(str(line_output2[i, 0]) + "," + str(line_output2[i, 1]) + "\n")

f.write("</coordinates>" + "\n"
        "</LineString>" + "\n"
        "</MultiGeometry>" + "\n"
        "<Style>" + "\n"
        "<LineStyle>" + "\n"
        "<color>" + kml_col + "</color>" + "\n"
        "<width>" + kml_size + "</width>" + "\n"
        "</LineStyle>" + "\n"
        "</Style>" + "\n"
        "</Placemark>" + "\n"
        "</Folder>" + "\n"
        "<Folder>" + "\n"
        "<name>" + file + "(" + root + ")" + "</name>" + "\n"
        "<open>0</open>" + "\n"
        "<visibility>1</visibility>" + "\n"
        "<Placemark>" + "\n"
        "<name>Line 3</name>" + "\n"
        "<MultiGeometry>" + "\n"
        "<LineString>" + "\n"
        "<altitudeMode>clampToGround</altitudeMode>" + "\n"
        "<coordinates>" + "\n")

for i in range(len(line_output3)):
     f.write(str(line_output3[i, 0]) + "," + str(line_output3[i, 1]) + "\n")

f.write("</coordinates>" + "\n"
        "</LineString>" + "\n"
        "</MultiGeometry>" + "\n"
        "<Style>" + "\n"
        "<LineStyle>" + "\n"
        "<color>" + kml_col + "</color>" + "\n"
        "<width>" + kml_size + "</width>" + "\n"
        "</LineStyle>" + "\n"
        "</Style>" + "\n"
        "</Placemark>" + "\n"
        "</Folder>" + "\n"
        "<Folder>" + "\n"
        "<name>" + file + "(" + root + ")" + "</name>" + "\n"
        "<open>0</open>" + "\n"
        "<visibility>1</visibility>" + "\n"
        "<Placemark>" + "\n"
        "<name>Line 4</name>" + "\n"
        "<MultiGeometry>" + "\n"
        "<LineString>" + "\n"
        "<altitudeMode>clampToGround</altitudeMode>" + "\n"
        "<coordinates>" + "\n")

for i in range(len(line_output4)):
     f.write(str(line_output4[i, 0]) + "," + str(line_output4[i, 1]) + "\n")

f.write("</coordinates>" + "\n"
        "</LineString>" + "\n"
        "</MultiGeometry>" + "\n"
        "<Style>" + "\n"
        "<LineStyle>" + "\n"
        "<color>" + kml_col + "</color>" + "\n"
        "<width>" + kml_size + "</width>" + "\n"
        "</LineStyle>" + "\n"
        "</Style>" + "\n"
        "</Placemark>" + "\n"
        "</Folder>" + "\n"
        "<Folder>" + "\n"
        "<name>" + file + "(" + root + ")" + "</name>" + "\n"
        "<open>0</open>" + "\n"
        "<visibility>1</visibility>" + "\n"
        "<Placemark>" + "\n"
        "<name>Line 5</name>" + "\n"
        "<MultiGeometry>" + "\n"
        "<LineString>" + "\n"
        "<altitudeMode>clampToGround</altitudeMode>" + "\n"
        "<coordinates>" + "\n")

for i in range(len(line_output5)):
     f.write(str(line_output5[i, 0]) + "," + str(line_output5[i, 1]) + "\n")

f.write("</coordinates>" + "\n"
        "</LineString>" + "\n"
        "</MultiGeometry>" + "\n"
        "<Style>" + "\n"
        "<LineStyle>" + "\n"
        "<color>" + kml_col + "</color>" + "\n"
        "<width>" + kml_size + "</width>" + "\n"
        "</LineStyle>" + "\n"
        "</Style>" + "\n"
        "</Placemark>" + "\n"
        "</Folder>" + "\n"
        "<Folder>" + "\n"
        "<name>" + file + "(" + root + ")" + "</name>" + "\n"
        "<open>0</open>" + "\n"
        "<visibility>1</visibility>" + "\n"
        "<Placemark>" + "\n"
        "<name>Line 6</name>" + "\n"
        "<MultiGeometry>" + "\n"
        "<LineString>" + "\n"
        "<altitudeMode>clampToGround</altitudeMode>" + "\n"
        "<coordinates>" + "\n")

for i in range(len(line_output6)):
     f.write(str(line_output6[i, 0]) + "," + str(line_output6[i, 1]) + "\n")

f.write("</coordinates>" + "\n"
        "</LineString>" + "\n"
        "</MultiGeometry>" + "\n"
        "<Style>" + "\n"
        "<LineStyle>" + "\n"
        "<color>" + kml_col + "</color>" + "\n"
        "<width>" + kml_size + "</width>" + "\n"
        "</LineStyle>" + "\n"
        "</Style>" + "\n"
        "</Placemark>" + "\n"
        "</Folder>" + "\n"
        "<Folder>" + "\n"
        "<name>" + file + "(" + root + ")" + "</name>" + "\n"
        "<open>0</open>" + "\n"
        "<visibility>1</visibility>" + "\n"
        "<Placemark>" + "\n"
        "<name>Line 7</name>" + "\n"
        "<MultiGeometry>" + "\n"
        "<LineString>" + "\n"
        "<altitudeMode>clampToGround</altitudeMode>" + "\n"
        "<coordinates>" + "\n")

for i in range(len(line_output7)):
     f.write(str(line_output7[i, 0]) + "," + str(line_output7[i, 1]) + "\n")

f.write("</coordinates>" + "\n"
        "</LineString>" + "\n"
        "</MultiGeometry>" + "\n"
        "<Style>" + "\n"
        "<LineStyle>" + "\n"
        "<color>" + kml_col + "</color>" + "\n"
        "<width>" + kml_size + "</width>" + "\n"
        "</LineStyle>" + "\n"
        "</Style>" + "\n"
        "</Placemark>" + "\n"
        "</Folder>" + "\n"
        "<open>1</open>" + "\n"
        "<visibility>1</visibility>" + "\n"
        "</Document>" + "\n"
        "</kml>" + "\n")

f.close()
#"""
# ##################################################################################################################################
