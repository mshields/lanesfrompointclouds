# AE107 Extracting Collateral for RT-Range from Point Clouds

## Scope
This project intends on taking a .las point cloud as input and outputting a RT-Range map file.

## Requirements
Recommend using [Anaconda](https://www.anaconda.com/distribution/) to manage your Python packages and install and reading in the AE107_env.yml file.

[oxt_transforms.py](https://gitlab.com/oxts/application-engineering/tools/file-processing/oxts-transform-python-module) is required.

las2txt.exe is also required, available as part of the [LAStools package.](https://www.cs.unc.edu/~isenburg/lastools/)

[OxTS GeoRef](https://gitlab.com/oxts/application-engineering/geocloud) is required.

[CloudCompare](https://www.danielgm.net/cc/) or a similar PointCloud viewer is required.

The Python code also makes good use of [SciKitLearn](https://scikit-learn.org/stable/), check it out, it's great!

**Lane Extraction  - *Location*.py** contains the main script for each of the survey locals.

Data used in the design challenge is available through the project folder on the O:/ drive (O:\Application Engineering\Projects\COMPLETED\AE107 Extracting Collateral for RT-Range from Point Clouds)

## Workflow

1. General OxTS GeoRef Work Flow

    1.1 Boresight - output .lir files

    1.2 Survey - output RTK NCOM, .pcap or .lcom, .lrf

    1.3 Generate PointCloud - output .las. This can be done either with the GUI or executable. If using the executable use the following args `geocloud.exe -boresight min_range=10 max_range=30 output_dir_path="C:/Your/ouput/path/"`

2. Pre-Processing

    2.1 Open in [CloudCompare](https://www.danielgm.net/cc/) and find intensity thresholds, road width, lane with and line width

    2.2 Convert .las to .txt - Use [LAStools](https://www.cs.unc.edu/~isenburg/lastools/) las2txt.exe to generate a .txt file with GPS time, X Pos, Y Pos, Z Pos and Intensity. 
    Arguements for `las2txt.exe -i *.las -parse txyzi -sep comma`

__At this point you ought to have the PointCloud as a .txt file with an associated local origin (.lrf file) and some noted values for the intensity thresholds, road width, lane with and line width.
All of these either read into the Python script or hard coded as globals in the options section.__

3. Python Script (steps are a breakdown of what is done in the code)

    3.1 Use [RANSAC](https://scikit-learn.org/stable/auto_examples/linear_model/plot_ransac.html) to fit a plane to the PointCloud and exclude all points within some distance to the plane.
        Given most of the points in your survey ought to lie on the ground this plane gives an estimate of where the ground is. Then only taking all the points that are within some distance of this will account for any curvature in the road surface.
    
    3.2 Filter the "ground points" based on intensity.
    
    3.3 Pre-process for unsupervised machine learning algorithms
        - order on X position
        - normalise and store scaling factor so we can reverse this later
    
    3.4 [DBSCAN](https://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html) with the radius value set to the distance between two lines (lane width).
        This is to act as a noise filter rather than for clustering and should only end up with one cluster covering all road markings.
    
    3.5 [RANSAC](https://scikit-learn.org/stable/auto_examples/linear_model/plot_ransac.html) line fit to whole dataset with the residual threshold window set at the road width.
        This ought to trim away any left over points that aren't on the road if we filter out all of the residuals.
    
    __At this point we ought to have clean data with with just the road markings before we move onto clustering the individual lanes.__
    
    3.6 Cluster individual lines using modified version of the popular [k-means](https://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html) clustering algorithm. Traditionally k-means finds k clusters in a given dataset by imposing k point centroids, computing the distance between each data point and the closest centroid, then re-estimating the centroids and repeating until convergence. The modified version implemented in the code here uses the distance to a line centroid rather than a point centroid.
    
    3.7 [RANSAC](https://scikit-learn.org/stable/auto_examples/linear_model/plot_ransac.html) line fitment for each cluster
    
    3.8 Reverse normalisation.
    
    3.9 Convert to Global frame.
    
    4.0 Export line files, map file and KML file.

## Further work
### Short Term
- Extend shape envelope mechanism to reduce noise points (use of priors of the lane geometry)
- Segment data in batches based on time as a way to discretise curves as multiple lines
- Move to fitting poly lines rather than straight lines to capture line curvature
- Validation using Leica total station as ground truth
- Comparison or fusion with RD330

### Long Term
- Extension to survey and classify other infrastructure objects (sign posts, etc.)
- Realtime processing to generate map files in real time, as a stepping stone to eventually work towards streaming lane positions into RT-Range in real time